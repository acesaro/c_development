#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int (*arithmetic_cb)(int num1, int num2);

int multiply(int num1, int num2)
{
  return num1 * num2;
}

int divide(int num1, int num2)
{
  if(num2 == 0)
  {
    return 0;
  }
  else
  {
    return num1 / num2;
  }
}

int add(int num1, int num2)
{
  return num1 + num2;
}

int subtract(int num1, int num2)
{
  return num1 - num2;
}

void arithmetic_function(int num1, int num2, arithmetic_cb arith_func)
{
  int result = arith_func(num1, num2);
  printf("%d\n", result);
}

int main(int argc, char *argv[])
{
  int num1, num2;
  char * arith_type = argv[3];
  (arithmetic_cb)arith_type;
  num1 = atoi(argv[1]);
  num2 = atoi(argv[2]);

  if(strcmp(arith_type, "add") == 0)
  {
    arithmetic_function(num1, num2, add);
  }
  else if(strcmp(arith_type, "subtract") == 0)
  {
    arithmetic_function(num1, num2, subtract);
  }
  else if(strcmp(arith_type, "multiply") == 0)
  {
    arithmetic_function(num1, num2, multiply);
  }
  else if(strcmp(arith_type, "divide") == 0)
  {
    arithmetic_function(num1, num2, divide);
  }
  else
  {
    printf("Unsupported function provided, exiting...\n\n");
  }
}

